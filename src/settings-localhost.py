ALLOWED_HOSTS = ['localhost','127.0.0.1']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'covid',
        'USER': 'postgres',
        'PASSWORD': 'p1234',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}
