# Generated by Django 3.0.4 on 2020-04-01 08:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('covid_backend', '0011_auto_20200401_0815'),
    ]

    operations = [
        migrations.RenameField(
            model_name='color',
            old_name='color',
            new_name='cluster_color',
        ),
    ]
