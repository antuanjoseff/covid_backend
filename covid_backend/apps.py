from django.apps import AppConfig


class CovidBackendConfig(AppConfig):
    name = 'covid_backend'
