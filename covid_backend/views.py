from django.shortcuts import render
import psycopg2
from django.conf import settings
from django.http import HttpResponse
import json
# Create your views here.

class CustomJSONEncoder(json.JSONEncoder):
    """Custom JSON Encoder."""

    def default(self, o):
        """Parse JSON attribute."""
        if isinstance(o, decimal.Decimal):
            # if it is decimal, float it
            return float(o)
        elif isinstance(o, datetime.datetime):
            # if it is datetime, return the isoformat
            return o.isoformat()
        else:
            # if it is another type of field, call the parent class
            return super(CustomJSONEncoder, self).default(o)


def index(request):
    """Geojson for covid data ."""
    db = settings.DATABASES['default']
    connection = psycopg2.connect(user=db['USER'],
                                  password=db['PASSWORD'],
                                  host=db['HOST'],
                                  port=db['PORT'],
                                  database=db['NAME'])
    cursor = connection.cursor()

    sql = ("SELECT ROW_TO_JSON(FC) FROM "
       "(SELECT 'FeatureCollection' AS type,"
       "(select array_agg(maxims) from ("
            "select max(pcr) as v from covid_backend_patientdata group by date order by date"
       ") as maxims) as maxims,"
       " coalesce(array_to_json(array_agg(f)),'[]') AS features FROM "
       "(SELECT 'Feature'as type, a.geom::json as geometry, "
            "(SELECT row_to_json(t) FROM ("
                "SELECT "
                    "(select min(recovered) from covid_backend_patientdata where ccaa_id=a.cod_ine and recovered>0) as first_recovered,"
                    "(select min(recovered) from covid_backend_patientdata where ccaa_id=a.cod_ine and recovered is not null) as first_recovered,"
                    "(select max(recovered) from covid_backend_patientdata where ccaa_id=a.cod_ine and recovered is not null) as last_recovered,"
                    "(select max(pcr) from covid_backend_patientdata) as maxvalue,"
                    "(select max(m)::integer as maxnormalized from (select max(pcr)/(inhabitants::float/100000) as m "
                        "from covid_backend_patientdata p, covid_backend_ccaa a where p.ccaa_id=a.cod_ine "
                        "group by inhabitants) as f),"
                    "(select max(inhabitants) from covid_backend_ccaa) as maxinhabitants,"
                    "a.inhabitants, a.name as name,"
                    "infected_color, hospitalized_color, active_color, "
                    "maxradius, recovered_color, icu_color, casualty_color,"
                    "infectec_not_hospitalized_color,"
                    "cluster_opacity, border_opacity, border_ratio,"
                    "frame_speed, max(infected) as total,"
                    "array_agg(infected order by date) as values, "
                    "array_agg(cases order by date) as cases, "
                    "array_agg(pcr order by date) as pcrs, "
                    "array_agg(test order by date) as tests, "
                    "array_agg(infected order by date) as infected, "
                    "array_agg(hospitalized order by date) as hospitalized, "
                    "array_agg(icu order by date) as icu, "
                    "array_agg(recovered order by date) as recovered, "
                    "array_agg(casualty order by date) as casualty, "
                    "array_agg(date order by date) as date FROM "
                "covid_backend_patientdata b "
                "WHERE b.ccaa_id=a.cod_ine GROUP BY ccaa_id, a.cod_ine order by date "
                ") AS t"
            ")as properties "
         "FROM covid_backend_ccaa a, covid_backend_color) AS f )AS fc")

    cursor.execute(sql)
    dades = cursor.fetchall()[0]

    return HttpResponse(json.dumps(dades[0], cls=CustomJSONEncoder),
                        content_type='application/json')

def daily(request):
    """Geojson for covid data ."""
    db = settings.DATABASES['default']
    connection = psycopg2.connect(user=db['USER'],
                                  password=db['PASSWORD'],
                                  host=db['HOST'],
                                  port=db['PORT'],
                                  database=db['NAME'])
    cursor = connection.cursor()

    sql = ("with taula as (select ccaa_id, date, "
                "pcr - lag(pcr, 1, 0) over (partition by ccaa_id order by ccaa_id, date) as pcr,"
                "recovered - lag(recovered, 1, 0) over (partition by ccaa_id order by ccaa_id, date) as recovered,"
                "hospitalized - lag(hospitalized, 1, 0) over (partition by ccaa_id order by ccaa_id, date) as hospitalized,"
                "icu - lag(icu, 1, 0) over (partition by ccaa_id order by ccaa_id, date) as icu,"
                "casualty - lag(casualty, 1, 0) over (partition by ccaa_id order by ccaa_id, date) as casualty"
                " from covid_backend_patientdata order by ccaa_id, date"
            ")"
            "select row_to_json(fc) "
            "   from("
                    "select a.cod_ine, a.name, "
                    "(select json_agg(ccaa) from (select * from taula where ccaa_id = a.cod_ine order by date) as ccaa"
                    ") as values"
            " from covid_backend_ccaa as a) as fc")

    cursor.execute(sql)
    # rows = [x for x in cursor]

    dades = cursor.fetchall()
    return HttpResponse(json.dumps(dades, cls=CustomJSONEncoder),
                        content_type='application/json')
