# Create your models here.
from djgeojson.fields import PointField
from django.db import models
# from colorfield.fields import ColorField
from colorful.fields import RGBColorField

class IntegerRangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)
    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)

class Ccaa(models.Model):
    """CCAA model."""

    name = models.CharField(max_length=256)
    cod_ine=models.IntegerField(primary_key=True)
    inhabitants = models.IntegerField(default=0)
    geom = PointField()

    class Meta:
        """Textos."""

        verbose_name = "Comunitat autònoma"
        verbose_name_plural = "Comunitats autònomes"

    def __str__(self):
        """Show name on lists."""
        return '%s' % (self.name)


class PatientData(models.Model):
    """CCAA model."""

    ccaa = models.ForeignKey(Ccaa, on_delete=models.CASCADE)
    date = models.DateField()
    infected = models.IntegerField(default=0, null=True)
    cases = models.IntegerField(default=0, null=True)
    pcr = models.IntegerField(default=0, null=True)
    test = models.IntegerField(default=0, null=True)    
    hospitalized = models.IntegerField(default=0, null=True)
    icu = models.IntegerField(default=0, null=True)
    recovered = models.IntegerField(default=0, null=True)
    casualty = models.IntegerField(default=0, null=True)

    class Meta:
        """Textos."""

        verbose_name = "Dada pacient"
        verbose_name_plural = "Dades pacients"

    def __str__(self):
        """Show name on lists."""
        return '%s (%s)' % (self.ccaa, self.date)


class Color(models.Model):
    """CCAA model."""

    maxradius = models.IntegerField(default=40, help_text="Valor enter")

    infected_color = RGBColorField(
                default='#fde333',
                help_text="Color dels infectats")

    active_color = RGBColorField(
                default='#fde333',
                help_text="Color dels infectats")

    hospitalized_color = RGBColorField(
                default='#fde333',
                help_text="Color dels hospitalitzats")

    recovered_color = RGBColorField(
                default='#fde333',
                help_text="Color de les altes")

    icu_color = RGBColorField(
                default='#fde333',
                help_text="Color de les UCI")

    casualty_color = RGBColorField(
                default='#fde333',
                help_text="Color de les defuncions")

    infectec_not_hospitalized_color = RGBColorField(
                default='#fde333',
                help_text="Color dels infectats no hospitalitzats")

    cluster_opacity = IntegerRangeField(
                default=1,
                min_value=0,
                max_value=100,
                help_text="Opacitat del cluster (%)")
    border_opacity = IntegerRangeField(
                default=1,
                min_value=0,
                max_value=100,
                help_text="Opacitat del border (%)")
    border_ratio = IntegerRangeField(
                default=30,
                min_value=0,
                max_value=100,
                help_text="Gruix del border en relació al cluster (%)")
    frame_speed = IntegerRangeField(
                default=200,
                min_value=0,
                max_value=10000,
                help_text="Milisegons entre frame i frame")

    class Meta:
        """Textos."""

        verbose_name = "Color"
        verbose_name_plural = "Colors"

    def __str__(self):
        """Show name on lists."""
        return '%s ' % (self.infected_color)
