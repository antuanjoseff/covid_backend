from django.contrib import admin

# Register your models here.

from leaflet.admin import LeafletGeoAdmin
from django.contrib import admin
from .models import Ccaa, PatientData, Color


class CcaaAdmin(LeafletGeoAdmin):
    ordering = ('cod_ine',)


class PatientDataAdmin(admin.ModelAdmin):
    ordering = ('-date', 'ccaa')

admin.site.register(Color)
admin.site.register(Ccaa, CcaaAdmin)
admin.site.register(PatientData, PatientDataAdmin)
